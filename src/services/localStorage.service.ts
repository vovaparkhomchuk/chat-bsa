import {saveUser} from "../store/reducers/users.reducer";
import {useDispatch} from "react-redux";

class LocalStorageService {
  getUser() {
    const userRaw = localStorage.getItem('user');
    if (userRaw) {
      const user = JSON.parse(userRaw);
      return user;
    }
  }
}

const localStorageService = new LocalStorageService();
export default localStorageService;
