import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Start from "./components/Start";
import {store} from './store/store';
import {Provider} from 'react-redux';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Start />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
