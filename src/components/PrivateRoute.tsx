import {useDispatch, useSelector} from "react-redux";
import {Navigate} from "react-router-dom";
import React, {ReactElement} from "react";
import localStorageService from "../services/localStorage.service";
import {saveUser} from "../store/reducers/users.reducer";
import Chat from "./Chat";

const PrivateRoute = ({children, chat}: { children: ReactElement, chat?: ReactElement }) => {
  const dispatch = useDispatch();
  const user = localStorageService.getUser();
  if (!user) return <Navigate to={'/login'} />
  dispatch(saveUser(user));

  if (user.token) {
    if (user.role === 'Admin') {
      return children;
    }
    else {
      return <Chat />;
    }
  }
  else return <Navigate to="/login"/>;
}

export default PrivateRoute;
