import {IMessage} from "../intefaces";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash, faEdit, faHeart} from '@fortawesome/free-solid-svg-icons'
import dayjs from "dayjs";
import {deleteMessage} from "../store/reducers/messages.reducer";
import {useDispatch} from "react-redux";
import EditMessage from "./EditMessage";
import {useState} from "react";

interface OwnMessageProps {
  idx: number,
  msg: IMessage;
}

const OwnMessage = ({idx, msg}: OwnMessageProps) => {
  const dispatch = useDispatch();
  const [showEdit, setShowEdit] = useState(false);

  const handleDeleteMessage = () => {
    dispatch(deleteMessage(msg.id));
  }

  const handleShowEdit = () => {
      setShowEdit(!showEdit);
  };

  return (
    <div className={'own-message'}>
      {showEdit && <EditMessage idx={idx} setShowEdit={setShowEdit} />}
      <div onClick={handleShowEdit} className={'message-edit'}>
        <FontAwesomeIcon icon={faEdit}/>
      </div>
      <div className={'message-text'}>
        {msg.text}
      </div>
      <div className={'message-time'}>{dayjs(msg.createdAt).format('HH:mm')}</div>
      <div onClick={handleDeleteMessage} className={'message-delete'}>
        <FontAwesomeIcon icon={faTrash}/>
      </div>
    </div>)
}

export default OwnMessage;
