import React, {useState, useEffect} from 'react';
import Preloader from "./Preloader";
import MessageList from "./MessageList";
import Header from "./Header";
import MessageInput from "./MessageInput";
import {IMessage} from "../intefaces";
import {fetchMessages} from "../store/reducers/messages.reducer";
import {useDispatch, useSelector} from "react-redux";

function Chat() {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [editMessage, setEditMessage] = useState<IMessage | {}>({});
  //@ts-ignore
  const {messages} = useSelector(state => state.messagesReducer);

  useEffect(() => {
    dispatch(fetchMessages());
  }, []);


  return (
    <div className="chat">
      {messages && messages.length > 0 ? (
        <>
          <Header messages={messages}/>
          <MessageList />
          <MessageInput
            messages={messages}
            // setMessages={setMessages}
            isEditing={isEditing}
            editMessage={editMessage}
            setIsEditing={setIsEditing}
            setEditMessage={setEditMessage} />
        </>
      ) : <Preloader/>}
    </div>
  );
}

export default Chat;
