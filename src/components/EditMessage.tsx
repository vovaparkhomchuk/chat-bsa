import {SetStateAction, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {editMessage} from "../store/reducers/messages.reducer";

const EditMessage = ({idx, setShowEdit}: { idx: number, setShowEdit: SetStateAction<any> }) => {
  const disptach = useDispatch();
  // @ts-ignore
  const message = useSelector(state => state.messagesReducer.messages[idx]);
  const [text, setText] = useState(message.text);

  const handleShowEdit = (event: any) => {
    if(event.target === event.currentTarget) {
      setShowEdit(false);
    }
  }

  const handleEdit = () => {
    disptach(editMessage({id: message.id, text}));
    setShowEdit(false);
  }

  return (
    <div onClick={handleShowEdit} className={'edit-message edit-message-modal modal-shown edit-message-close'}>
      <div className={'edit-block'}>
        <input className={'message-input-text edit-message-input'} value={text} onChange={e => setText(e.target.value)}/>
        <div onClick={handleEdit} className={'message-input-button edit-message-button'}>Save</div>
      </div>
    </div>
  )
}

export default EditMessage;
