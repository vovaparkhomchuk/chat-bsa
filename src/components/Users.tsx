import {useEffect} from "react";
import {fetchUsers} from "../store/reducers/users.reducer";
import {useDispatch, useSelector} from "react-redux";
import Preloader from "./Preloader";
import {deleteUser} from "../store/reducers/users.reducer";

const Users = () => {
  const dispatch = useDispatch()
  // @ts-ignore
  const {users, status} = useSelector(state => state.usersReducer);

  const handleDeleteUser = (id: any) => {
    dispatch(deleteUser(id))
  }

  useEffect(() => {
    dispatch(fetchUsers())
  }, [])

  if (status !== 'fulfilled') return <Preloader />

  return (
    <div className={'users'}>
      {users && users.length > 0 && users.map((user: any) => (
        <div className={'user'}>
          <img src={user.avatar}/>
          <div className={'user-info'}>
            <span>{user.name}</span>
            <span>{user.email}</span>
            <span>{user.role}</span>
          </div>
          <div>
            {user.role !== 'Admin' && <div onClick={() => handleDeleteUser(user.id)}>Delete</div>}
          </div>

        </div>
      ))}
    </div>
  )
}

export default Users


// {
//   "id":1,
//   "name":"admin",
//   "email":"admin",
//   "role":"Admin",
//   "avatar":"https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA"
// }
