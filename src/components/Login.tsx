import {useEffect, useState} from "react";
import axios from 'axios';
import {useDispatch} from "react-redux";
import {saveUser} from "../store/reducers/users.reducer";
import {useNavigate} from "react-router-dom";
import localStorageService from "../services/localStorage.service";

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    try {
      const {data} = await axios.post('https://bsa-chat.azurewebsites.net/api/Auth/login', {password, email: userName});
      dispatch(saveUser(data));
      localStorage.setItem('user', JSON.stringify(data));
      redirect(data);
    } catch (e) {
      console.error({e});
    }
  }

  useEffect(() => {
    const user = localStorageService.getUser();
    if (user) {
      dispatch(saveUser(user));
      redirect(user);
    }
  }, [])

  const redirect = (data: {token: string, role: string}) => {
    if (data.token) {
      if (data.role === 'Admin') navigate('/users');
      // else navigate('/users');
      else navigate('/');
    }
  }

  return (
    <div className={'login'}>
      <input className={'message-input-text'}
             value={userName}
             placeholder={'User name'}
             onChange={e => setUserName(e.target.value)}/>
      <input className={'message-input-text'}
             value={password}
             type={'password'}
             placeholder={'Password'}
             onChange={e => setPassword(e.target.value)}/>
      <div onClick={handleLogin} className={'message-input-button'}>Login</div>
    </div>
  )
}

export default Login;
