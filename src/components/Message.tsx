import {IMessage, IMessages} from "../intefaces";
import dayjs from "dayjs";
import {useDispatch} from "react-redux";
import {likeMessage} from "../store/reducers/messages.reducer";

const Message = ({msg}: { msg: IMessage }) => {
  const dispatch = useDispatch();
  const handleLike = () => {
    dispatch(likeMessage(msg.id));
  }

  return (
    <div className={'message'}>
      <div className={'message-user-avatar'}>
        <img src={msg.avatar}/>
      </div>
      <div className={'message-center'}>
        <div className={'message-user-name'}>
          {msg.user}
        </div>
        <div className={'message-text'}>
          {msg.text}
        </div>
        <div onClick={handleLike} className={msg.liked ? 'message-liked' : 'message-like'}>
          {msg.liked ? 'Liked!' : 'Like'}
        </div>
      </div>
      <div className={'message-time'}>{dayjs(msg.createdAt).format('HH:mm')}</div>
    </div>)
}

export default Message;
