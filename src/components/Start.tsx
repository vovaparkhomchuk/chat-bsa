import {
  BrowserRouter,
  Routes,
  Route,
  useLocation,
  useNavigate,
  Navigate
} from "react-router-dom";
import PrivateRoute from "./PrivateRoute";
import Chat from "./Chat";
import Login from "./Login";
import Users from "./Users";
import localStorageService from "../services/localStorage.service";


const Start = () => {
  const user = localStorageService.getUser();

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Login/>}/>
        <Route path="/users" element={
          <PrivateRoute>
            <Users />
          </PrivateRoute>
        }/>
        <Route path="/*" element={
          <PrivateRoute>
            <Chat/>
          </PrivateRoute>}/>
      </Routes>
    </BrowserRouter>
  )
}

export default Start;
