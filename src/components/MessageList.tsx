import {useEffect} from 'react';
import Message from "./Message";
import OwnMessage from "./OwnMessage";
import {useSelector} from "react-redux";
import Preloader from "./Preloader";


const MessageList = () => {
  //@ts-ignore
  const {name} = useSelector(state => state.usersReducer.user);
  //@ts-ignore
  const {messages, status} = useSelector(state => state.messagesReducer);

  useEffect(() => {
    if (messages.length > 0) {
      const list = document.getElementsByClassName('message-list')[0];
      list.scrollTop = list.scrollHeight;
    }
  }, [messages.length])

  if (status !== 'fulfilled') return <Preloader/>;

  return (
    <div className={'message-list'}>
      {// @ts-ignore
        messages.map((msg, idx) => {
            let newDay = false;
            let day;
            const current = new Date(msg.createdAt);

            if (messages[idx - 1]) {
              const previous = new Date(messages[idx - 1].createdAt);
              if (current.getDate() !== previous.getDate()) {
                newDay = true;
                if (current.getDate() === new Date().getDate()) day = "Today";
                else if (current.getDate() === new Date().getDate() + 1) day = "Yesterday";
                else day = `${current.toLocaleDateString("en", {weekday: 'long'})}, ${current.getDate()} ${current.toLocaleString('en', {month: 'long'})}`
              }
            } else {
              newDay = true;
              day = `${current.toLocaleDateString("en", {weekday: 'long'})}, ${current.getDate()} ${current.toLocaleString('en', {month: 'long'})}`
            }

            return (
              msg.user === name ?
                (<div key={idx}>
                  {newDay ? (<div className={'messages-divider'}>{day}</div>) : null}
                  <OwnMessage
                    idx={idx}
                    msg={msg}
                    key={msg.id}/>
                </div>) :
                (<div key={idx}>
                  {newDay ? (<div className={'messages-divider'}>{day}</div>) : null}
                  <Message
                    key={msg.id}
                    msg={msg}/>
                </div>)
            )
          }
        )}
    </div>
  )
}
export default MessageList;
