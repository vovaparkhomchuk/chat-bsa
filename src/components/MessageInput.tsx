import React, {SetStateAction, useState, useEffect} from "react";
import {IMessage} from "../intefaces";
import {useDispatch, useSelector} from "react-redux";
import {sendMessage} from "../store/reducers/messages.reducer";


interface MessageInputProps {
  messages: IMessage[];
  setMessages?: SetStateAction<any>;
  isEditing: boolean;
  editMessage: IMessage | {};
  setIsEditing: SetStateAction<any>;
  setEditMessage: SetStateAction<any>;
}

const MessageInput = ({messages, setMessages, isEditing, editMessage, setIsEditing, setEditMessage}: MessageInputProps) => {
  const [text, setText] = useState("");
  const dispatch = useDispatch();

  const inputHandler = (e: { target: { value: SetStateAction<string>; }; }) => setText(e.target.value);

  const handleSendMessage = () => {
    if (text === '') return;
    dispatch(sendMessage(text));
    setText('');
  };

  const saveEdit = () => {
    if (text === '') return;
    const newMessages = messages.map(msg => {
      if ('id' in editMessage && msg.id === editMessage.id) return {...editMessage, text};
      return msg;
    });
    setMessages([...newMessages]);
    setText('');
    setIsEditing(false);
    setEditMessage({});
  };

  useEffect(() => {
    if (isEditing && 'text' in editMessage) setText(editMessage.text);
  }, [isEditing]);

  return (
    <div className={'message-input'}>
      <input className={'message-input-text'} onChange={inputHandler} value={text}/>
      <div className={'message-input-button'} onClick={isEditing ? saveEdit : handleSendMessage}>{isEditing ? 'Save' : 'Send'}</div>
    </div>
  )
}

export default MessageInput;
