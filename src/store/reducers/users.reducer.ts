import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import axios from "axios";
import dayjs from "dayjs";

const initialState = {
  user: {},
  token: null,
  role: null,
  users: [],
  status: null,
}

export const fetchUsers = createAsyncThunk(
  "users/fetch",
  async (arg, {getState}) => {
    const state = getState();
    try {
      const {data} = await axios.get('https://bsa-chat.azurewebsites.net/api/Users', {
        headers: {
          "Content-type": "application/json",
          // @ts-ignore
          "Authorization": `Bearer ${state.usersReducer.user.token}`,
        }
      })
      // @ts-ignore
      return data
    } catch (error) {
      console.error(error);
    }
  }
);

export const deleteUser = createAsyncThunk(
  "users/delete",
  async (id: string | number, {getState}) => {
    const state = getState();
    try {
      const {data} = await axios.delete(`https://bsa-chat.azurewebsites.net/api/Users/${id}`, {
        headers : {
          // @ts-ignore
          'Authorization' : `Bearer ${state.usersReducer.user.token}`
        }
      })
      // @ts-ignore
      return id;
    } catch (error) {
      console.error({error});
    }
  }
);


const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    saveUser: (state, action) => {
      state.user = action.payload;
    },
  },
  extraReducers: {
    // @ts-ignore
    [fetchUsers.fulfilled]: (state, action) => {
      state.status = 'fulfilled'
      state.users = action.payload
    },
    // @ts-ignore
    [fetchUsers.pending]: (state) => {
      state.status = 'pending'
    },
    // @ts-ignore
    [fetchUsers.rejected]: (state) => {
      state.status = 'rejected'
    },
    // @ts-ignore
    [deleteUser.fulfilled]: (state, action) => {
      state.users = state.users.filter((user: any) => user.id !== action.payload);
    },
  }
});

export const {saveUser} = usersSlice.actions;
export default usersSlice.reducer;
