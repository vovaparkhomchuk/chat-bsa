import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {IMessages} from "../../intefaces";
import axios from "axios";
import dayjs from 'dayjs';

interface IMessagesRedcuder {
  messages: IMessages;
  status: null,
}

const initialState = {
  messages: []
};

export const fetchMessages = createAsyncThunk(
  "messages/fetch",
  async (arg, {getState}) => {
    const state = getState();
    try {
      const {data} = await axios.get('https://bsa-chat.azurewebsites.net/api/Messages', {
        headers: {
          "Content-type": "application/json",
          // @ts-ignore
          "Authorization": `Bearer ${state.usersReducer.user.token}`,
        }
      })
      // @ts-ignore
      return data.sort((a,b) => dayjs(a.createdAt) - dayjs(b.createdAt))
    } catch (error) {
      console.error(error);
    }
  }
);

export const sendMessage = createAsyncThunk(
  "messages/send",
  async (text: string, {getState}) => {
    const state = getState();
    try {
      const {data} = await axios.post('https://bsa-chat.azurewebsites.net/api/Messages', {
        text,
      },{
        headers : {
          'Content-Type' : 'application/json',
          'Accept' : 'application/json',
          // @ts-ignore
          'Authorization' : `Bearer ${state.usersReducer.user.token}`
        }
      })
      // @ts-ignore
      return data;
    } catch (error) {
      console.error({error});
    }
  }
);

export const deleteMessage = createAsyncThunk(
  "messages/delete",
  async (id: string | number, {getState}) => {
    const state = getState();
    try {
      const {data} = await axios.delete(`https://bsa-chat.azurewebsites.net/api/Messages/${id}`, {
        headers : {
          // @ts-ignore
          'Authorization' : `Bearer ${state.usersReducer.user.token}`
        }
      })
      // @ts-ignore
      return id;
    } catch (error) {
      console.error({error});
    }
  }
);

export const likeMessage = createAsyncThunk(
  "messages/like",
  async (id: string | number, {getState}) => {
    const state = getState();
    let msg;
    let msgId;
    // @ts-ignore
    state.messagesReducer.messages.map((message: any, idx: any) => {
      if (message.id === id) {
        msg = message;
        msgId = idx;
      }
    })
    try {

      const {data} = await axios.put(`https://bsa-chat.azurewebsites.net/api/Messages/${id}`, {
        // @ts-ignore
        liked: !msg.liked,
      },{
        headers : {
          // @ts-ignore
          'Authorization' : `Bearer ${state.usersReducer.user.token}`
        }
      })
      // @ts-ignore
      return {id, msgId};
    } catch (error) {
      console.error({error});
    }
  }
);

export const editMessage = createAsyncThunk(
  "messages/edit",
  async ({id, text}: {id: number, text: string}, {getState}) => {
    const state = getState();
    let msg;
    let msgId;
    // @ts-ignore
    state.messagesReducer.messages.map((message: any, idx: any) => {
      if (message.id === id) {
        msg = message;
        msgId = idx;
      }
    })
    try {

      const {data} = await axios.put(`https://bsa-chat.azurewebsites.net/api/Messages/${id}`, {
        // @ts-ignore
        text: text,
      },{
        headers : {
          // @ts-ignore
          'Authorization' : `Bearer ${state.usersReducer.user.token}`
        }
      })
      // @ts-ignore
      return {msgId, text};
    } catch (error) {
      console.error({error});
    }
  }
);


const messagesSlice = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    addNewMessage: (state, action) => {
      state.messages.push(action.payload as never);
    }
  },
  extraReducers: {
    // @ts-ignore
    [fetchMessages.fulfilled]: (state, action) => {
      state.status = 'fulfilled';
      state.messages = action.payload;
    },
    // @ts-ignore
    [fetchMessages.pending]: (state, action) => {
      state.status = 'pending';
    },
    // @ts-ignore
    [fetchMessages.rejected]: (state, action) => {
      state.status = 'rejected';
    },
    // @ts-ignore
    [sendMessage.fulfilled]: (state, action) => {
      state.messages.push(action.payload);
    },
    // @ts-ignore
    [deleteMessage.fulfilled]: (state, action) => {
      state.messages = state.messages.filter((msg: any) => msg.id !== action.payload);
    },
    // @ts-ignore
    [likeMessage.fulfilled]: (state, action) => {
      const {id, msgId} = action.payload;
      // @ts-ignore
      state.messages[msgId].liked = !state.messages[msgId].liked;
    },
    // @ts-ignore
    [editMessage.fulfilled]: (state, action) => {
      const {msgId, text} = action.payload;
      // @ts-ignore
      state.messages[msgId].text = text;
    },
  }
})

export const {addNewMessage} = messagesSlice.actions;
export default messagesSlice.reducer;
