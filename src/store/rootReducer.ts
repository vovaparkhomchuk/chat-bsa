import messagesReducer from "./reducers/messages.reducer";
import usersReducer from "./reducers/users.reducer";

const rootReducer = {messagesReducer, usersReducer};
export default rootReducer;
